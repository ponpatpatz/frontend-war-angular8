import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { StargazersComponent } from './stargazers/stargazers.component';


const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: ':owner/:repo', component: StargazersComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
