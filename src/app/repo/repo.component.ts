import { Component, OnChanges, SimpleChanges, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'repo',
  templateUrl: './repo.component.html',
  styleUrls: ['./repo.component.scss']
})
export class RepoComponent implements OnChanges {
  @Input() repo?: string;
  info: any;

  constructor(private http: HttpClient) { }

  async ngOnChanges(changes: SimpleChanges) {
    let repo = changes['repo'].currentValue;
    this.GET_info(repo);
  }


  async  GET_info(repo) {
    try {
      let header = {
        headers: {
          'Authorization': `token fbedabf134e561e8515b0c939d4f7b7b17f749ff`
        }
      }
      const rs: any = await this.http.get(`https://api.github.com/repos/${repo}`, header).toPromise();
      this.info = rs;
    } catch (error) {
      this.info = null
    }

  }



}
