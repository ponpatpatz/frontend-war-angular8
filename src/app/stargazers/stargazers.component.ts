import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'stargazers',
  templateUrl: './stargazers.component.html',
  styleUrls: ['./stargazers.component.scss']
})
export class StargazersComponent implements OnInit, OnDestroy {
  sub: any;
  stargazers: any;
  repoName: string;

  constructor(
    private http: HttpClient,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.sub = this.activatedRoute.params.subscribe(params => {
      let owner = params['owner'];
      let repo = params['repo'];
      this.repoName = `${owner}/${repo}`;
      this.GET_stargazers(owner, repo);
      // In a real app: dispatch action to load the details here.
    });
  }

  async GET_stargazers(owner, repo) {
    try {
      let header = {
        headers: {
          'Authorization': `token fbedabf134e561e8515b0c939d4f7b7b17f749ff`
        }
      }
      const rs: any = await this.http.get(`https://api.github.com/repos/${owner}/${repo}/stargazers`, header).toPromise();
      this.stargazers = rs;
    } catch (error) {
      this.stargazers = null;
    }


  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
